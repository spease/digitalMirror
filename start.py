#!/usr/bin/env python
"""
This file is part of digitalMirror.

    digitalMirror is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    digitalMirror is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with digitalMirror.  If not, see <http://www.gnu.org/licenses/>.
"""

# Import general modules
import pygame
import os
import sys
import Queue
import threading
from defaultmodules import threads
from defaultmodules import modules
from defaultmodules import settings

def main():
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)

    pygame.init()
    width=1280
    height=1024
    screen=pygame.display.set_mode([width,height])
    pygame.mouse.set_visible(False)
    pygame.display.set_caption("digitalMirror")
    clock=pygame.time.Clock()
    pygame.event.set_allowed([pygame.KEYDOWN, pygame.KEYUP])

    background = pygame.Surface(screen.get_size())
    background.convert()
    background.fill((0,0,0))
    screen.blit(background,(0,0))
    pygame.display.flip()

    digi_restart = False
    logicQueueObject=None
    logicQueue = Queue.Queue()
    offloadQueue=Queue.Queue()

    threadstop=threading.Event()
    recordEvent=threading.Event()

    offloadThread = threading.Thread(target=threads.OffloaderThread, args=(logicQueue,offloadQueue, threadstop))
    offloadThread.daemon=True
    offloadThread.start()

    #recorderThread = threading.Thread(target=threads.RecorderThread, args=(recordEvent, logicQueue, threadstop))
    #recorderThread.daemon=True
    #recorderThread.start()
    
    offloadQueue.put(('getWeather', settings.weatherURL))
    offloadQueue.put(('getCalendar', settings.calendarURL))

    moduleList = []
    moduleList.append(modules.DateModule(50,25))
    #moduleList.append(modules.FPSModule(10, height-50))
    moduleList.append(modules.WeatherModule(width-50, 22))
    moduleList.append(modules.CalendarModule(50, 156))
    moduleList.append(modules.IconModule(width-110,height-110))

    weatherObject=None
    calendarObject=None
    iconObject=0
    
    weatherEvent  = pygame.USEREVENT
    calendarEvent = pygame.USEREVENT+1
    pingEvent     = pygame.USEREVENT+2
    
    pygame.time.set_timer(calendarEvent, 500000)
    pygame.time.set_timer(weatherEvent, 300000)
    pygame.time.set_timer(pingEvent, 10000)

    sprites = modules.ModuleContainer(moduleList)

    # -------------Main Event Loop ---------------
    while digi_restart == False:
        # Limit to 30 frames per second
        clock.tick(30)
        
        try:
            logicQueueObject=logicQueue.get_nowait()
        except Queue.Empty:
            pass
        else:
            #Handle our task
            if logicQueueObject[0]=='getWeather':
                weatherObject=logicQueueObject[1]
                pygame.time.set_timer(weatherEvent, 500000)
            elif logicQueueObject[0]=='getCalendar':
                calendarObject=logicQueueObject[1]
                pygame.time.set_timer(calendarEvent, 600000)
            elif logicQueueObject[0]=='getPing':
                if logicQueueObject[1]==True:
                    iconObject = iconObject if iconObject >0  else 0
                else:
                    iconObject = -1
                pygame.time.set_timer(pingEvent, 10000)
            elif logicQueueObject[0]=='getUpdate' or logicQueueObject[0]=='powerDown':
                digi_restart=True # Flag that we are done so we exit
            elif logicQueueObject[0]=='Response':
                print logicQueueObject[1]
            elif logicQueueObject[0]=='MicControl':
                iconObject=logicQueueObject[1]

        screen.fill((0,0,0));

        sprites.update(WeatherModule=weatherObject, CalendarModule=calendarObject, IconModule=iconObject)
        sprites.draw(screen)
        pygame.display.flip()

        
        for event in pygame.event.get(): # User did something
            if event.type == pygame.KEYDOWN:
                if event.key == (pygame.K_F11):
                    digi_restart=True # Flag that we are done so we exit
                elif event.key == (pygame.K_F8):
                    offloadQueue.put(('powerDown', ""))
                elif event.key == (pygame.K_F12):
                    offloadQueue.put(('getUpdate', ""))
                elif event.key == (pygame.K_SPACE) and iconObject != -1 :
                    recordEvent.set()
            elif event.type == weatherEvent:
                offloadQueue.put(('getWeather', settings.weatherURL))
            elif event.type == calendarEvent:
                offloadQueue.put(('getCalendar', settings.calendarURL))
            elif event.type == pingEvent:
                offloadQueue.put(('getPing', ""))
            elif event.type == pygame.QUIT:
                digi_restart=True
    # Be IDLE friendly. If you forget this line, the program will 'hang'
    # on exit.
    threadstop.set()
    recordEvent.set()  # Unlock the record thread so it can shut down too.
    #recorderThread.join()
    offloadThread.join()
    
    pygame.quit ()

if __name__ == '__main__':
        main()
