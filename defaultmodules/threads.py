#!/usr/bin/env python
"""
This file is part of digitalMirror.

    digitalMirror is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    digitalMirror is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with digitalMirror.  If not, see <http://www.gnu.org/licenses/>.
"""

import threading
import Queue
import requests
import json
import settings
import datetime
from subprocess import Popen
import alsaaudio
from audioop import rms
import httplib2
import os
from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools



class RecorderThread(threading.Thread):    
    def __init__(self, recordEvent, logicQueue, stop):
        threading.Thread.__init__(self)
        self.stop = stop
        self.recordEvent=recordEvent
        self.logicQueue=logicQueue
        self.THRESHOLD = settings.speakingThreshold
        self.CHUNK_SIZE = 128

        self.input = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, mode=alsaaudio.PCM_NORMAL)
        self.input.setchannels(1)
        self.input.setrate(16000)
        self.input.setformat(alsaaudio.PCM_FORMAT_S16_LE)
        self.input.setperiodsize(self.CHUNK_SIZE)

        while not self.stop.isSet():
            self.recordEvent.wait()
            if not self.stop.isSet():
                headers = {'Authorization': 'Bearer ' + settings.witaikey,
                           'Content-Type': 'audio/raw; encoding=signed-integer; '+ 
                           'bits=16; rate=16000; endian=little', 'Transfer-Encoding':
                           'chunked'}
                url = 'https://api.wit.ai/speech'
                self.response = requests.post(url, headers=headers, data=self.gen()).text
                self.logicQueue.put(("Response",self.response))
                self.logicQueue.put(("MicControl",0))
            self.recordEvent.clear()
        print "Recorder thread ending."


    # Yields chunks for requests to send out.
    def gen(self):
        num_silent = 0
        snd_started = False
        start_pack = 0
        counter = 0
        #print "Microphone on!"
        self.logicQueue.put(("MicControl",1))
        
        i = 0
        data = []

        while 1:
            l, snd_data = self.input.read()
            data.append(snd_data)

            rms, silent = self.is_silent(snd_data)

            if silent and snd_started:
                num_silent += 1

            elif not silent and not snd_started:
                i = len(data) - self.CHUNK_SIZE*2  # Set the counter back a few seconds
                if i < 0:                     # so we can hear the start of speech.
                    i = 0
                snd_started = True
                self.logicQueue.put(("MicControl",2))
                #print "TRIGGER at " + str(rms) + " rms."

            elif not silent and snd_started and not i >= len(data):
                i, temp = self.returnUpTo(i, data, 16)
                yield temp
                num_silent = 0

            if snd_started and num_silent > 100:
                #print "Stop Trigger"
                break

            if counter > 1000:  # Slightly less than 10 seconds.
                #print "Timeout, Stop Trigger"
                break

            if snd_started:
                counter = counter + 1

        # Yield the rest of the data.
        #print "Pre-streamed " + str(i) + " of " + str(len(data)) + "."
        self.logicQueue.put(("MicControl",3))
        while (i < len(data)):
            i, temp = self.returnUpTo(i, data, 128)
            yield temp
        #print "Swapping to thinking."


    # Returns if the RMS of block is less than the threshold
    def is_silent(self, block):
        try:
            rms_value = rms(block, 2)
            return rms_value, rms_value <= self.THRESHOLD
        except:
            return -1, True
            # Sometimes during loud noise you don't get the full
            # number of blocks, which causes rms to bug out.


    # Returns as many (up to returnNum) blocks as it can.
    def returnUpTo(self, iterator, values, returnNum):
        if iterator+returnNum < len(values):
            return (iterator + returnNum,
                    "".join(values[iterator:iterator + returnNum]))
        else:
            temp = len(values) - iterator
            return (iterator + temp + 1, "".join(values[iterator:iterator + temp]))

            
class OffloaderThread(threading.Thread):
    def __init__(self, logicQueue, offloadQueue, stop):
        threading.Thread.__init__(self)
        self.amOnline=False
        self.logicQueue=logicQueue
        self.offloadQueue=offloadQueue
        
        self.stop=stop
        self.execute = {'getWeather' : self.getWeather,
                        'getURL'     : self.getURL,
                        'getCalendar': self.getCalendar,
                        'getPing'    : self.getPing,
                        'getUpdate'  : self.getUpdate,
                        'powerDown'  : self.powerDown}
        self.ret=None

        self.service = discovery.build('calendar', 'v3', http=self.get_credentials().authorize(httplib2.Http()))
        
        while not self.stop.isSet():
            try:
                (option, argument) = self.offloadQueue.get(True,4)
                self.offloadQueue.task_done()
                self.ret=None
                self.execute[option](argument)
                if self.amOnline==False: 
                    self.logicQueue.put(('getPing',False))
                self.logicQueue.put((option,self.ret))
            except Queue.Empty:
                pass
        print "Offloader thread ending."
        
    def get_credentials(self):
        credential_path = os.path.join(os.path.expanduser('~'), '.credentials','calendar-quickstart.json')
        credentials = oauth2client.file.Storage(credential_path).get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(os.path.join('defaultmodules','assets','client_secret.json'), 'https://www.googleapis.com/auth/calendar.readonly')
            flow.user_agent = 'digitalMirror'
        return credentials
    
    def getPing(self,argument):
        try:
            r = requests.get("http://google.com")
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            #Network Error?
            self.amOnline=False
        except:
            self.amOnline=False
        else:
            self.amOnline=True
        self.ret = self.amOnline

    def getUpdate(self, argument):
        Popen(["./getUpdate.sh"])
        self.ret=True

    def powerDown(self, argument):
        Popen(["./powerDown.sh"])
        self.ret=True
            
    def getWeather(self,argument):
        try:
            r = requests.get(argument)
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            self.amOnline=False
        except:
            self.amOnline=False
        else:
            self.amOnline=True
            self.ret = r.json()
            weather = (self.ret['daily']['data'][0]['sunriseTime'], self.ret['daily']['data'][0]['sunsetTime'], str(int(round(self.ret['currently']['windSpeed']))),str(int(round(self.ret['currently']['temperature']))), self.ret['currently']['icon'])
            forecast = ((self.ret['daily']['data'][0]['time'],self.ret['daily']['data'][0]['icon'],self.ret['daily']['data'][0]['temperatureMin'],self.ret['daily']['data'][0]['temperatureMax']),
                        (self.ret['daily']['data'][1]['time'],self.ret['daily']['data'][1]['icon'],self.ret['daily']['data'][1]['temperatureMin'],self.ret['daily']['data'][1]['temperatureMax']),
                        (self.ret['daily']['data'][2]['time'],self.ret['daily']['data'][2]['icon'],self.ret['daily']['data'][2]['temperatureMin'],self.ret['daily']['data'][2]['temperatureMax']),
                        (self.ret['daily']['data'][3]['time'],self.ret['daily']['data'][3]['icon'],self.ret['daily']['data'][3]['temperatureMin'],self.ret['daily']['data'][3]['temperatureMax']),
                        (self.ret['daily']['data'][4]['time'],self.ret['daily']['data'][4]['icon'],self.ret['daily']['data'][4]['temperatureMin'],self.ret['daily']['data'][4]['temperatureMax']),
                        (self.ret['daily']['data'][5]['time'],self.ret['daily']['data'][5]['icon'],self.ret['daily']['data'][5]['temperatureMin'],self.ret['daily']['data'][5]['temperatureMax']),
                        (self.ret['daily']['data'][6]['time'],self.ret['daily']['data'][6]['icon'],self.ret['daily']['data'][6]['temperatureMin'],self.ret['daily']['data'][6]['temperatureMax']))
            alerts = (None) #TODO: Add in alerts.
            self.ret = (weather,forecast, alerts)
            

    def getURL(self, argument):
        try:
            r = requests.get(argument)
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            #Network Error?
            self.amOnline=False
        else:
            self.amOnline=True
            self.ret = r.content

    def getCalendar(self, argument):
        try:
            now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
            eventsResult = self.service.events().list(
                calendarId='primary', timeMin=now, maxResults=7, singleEvents=True,
                orderBy='startTime').execute()
            events = eventsResult.get('items', [])
        except:
            #Network Error?
            self.amOnline=False
        else:
            self.amOnline=True
            self.ret=[]

            for event in events:
                eventStartTime=event['start'].get('dateTime')
                d0=datetime.datetime.strptime(event['start'].get('date'), "%Y-%m-%d").date()
                eventStartTime=(eventStartTime+" " if eventStartTime else "")
                self.ret.append((eventStartTime+event['summary'], (d0-datetime.date.today()).days))

