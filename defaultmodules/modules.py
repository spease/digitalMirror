#!/usr/bin/env python
"""
This file is part of digitalMirror.

    digitalMirror is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    digitalMirror is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with digitalMirror.  If not, see <http://www.gnu.org/licenses/>.
"""

import pygame
import datetime
from collections import defaultdict
import settings
import os

class ModuleContainer():
    def __init__(self, modules):
        self.modules=modules
    def update(self, **kwargs):
        for f in self.modules:
            if f.name in kwargs:
                f.update(kwargs[f.name])
            else:
                f.update()      
    def draw(self, screen):
        for f in self.modules:
            f.draw(screen)
    def add(self, moreModules):
        self.modules = self.modules + moreModules

class CalendarModule(pygame.sprite.OrderedUpdates):
    class CalendarSprite(pygame.sprite.Sprite):
        def __init__(self, x_pos, y_pos):
            pygame.sprite.Sprite.__init__(self)
            self.font = pygame.font.Font(settings.boldfont, 17)
            self.image=pygame.Surface((1,1)).convert()
            self.image.fill((0,0,0))
            self.rect=self.image.get_rect()
            self.x_pos=x_pos
            self.y_pos=y_pos
            self.rect.topleft=self.x_pos,self.y_pos
            self.then = None
            self.calendarObject = None
            self.hash = None
            self.maxWidth=0
            self.calHold=[]
            self.string=""
        def update(self, *args):  #self.calendarObject[i] where i is number of events <=7 = (name, # days until)
            if args[0]:
                self.calendarObject=args[0]
                self.hash = self.calendarObject[0][0] + "" + str(datetime.datetime.now().day)
            if self.then!=self.hash:
                self.maxWidth=0
                self.calHold=[]
                for i in range(0,len(self.calendarObject)):
                    self.calHold.append(self.font.render(self.calendarObject[i][0], 1, (255-(i*35),255-(i*35),255-(i*30)), (0,0,0)).convert())
                    if self.maxWidth < self.calHold[i].get_width():
                        self.maxWidth = self.calHold[i].get_width()
                    
                self.image=pygame.Surface((self.maxWidth+20+100,25*len(self.calendarObject))).convert()
                for i in range(0,len(self.calendarObject)):
                    self.image.blit(self.calHold[i], (0, i*25))
                    if self.calendarObject[i][1] == 0:
                        self.string="Today"
                    elif self.calendarObject[i][1] == 1:
                        self.string="Tommorow"
                    else:
                        self.string=str(self.calendarObject[i][1])+" days"
                    self.image.blit(self.font.render(self.string, 1, (255-(i*35),255-(i*35),255-(i*30)), (0,0,0)).convert(), (self.maxWidth+20,i*25))
                    
                self.then=self.hash
                self.rect=self.image.get_rect()
                self.rect.topleft=self.x_pos,self.y_pos

    def __init__(self, x_pos, y_pos):
        self.name="CalendarModule"
        self.calendarSprite = self.CalendarSprite(x_pos, y_pos)
        pygame.sprite.OrderedUpdates.__init__(self, (self.calendarSprite))

class WeatherModule(pygame.sprite.OrderedUpdates):
    class PoweredBySprite(pygame.sprite.Sprite):
        def __init__(self, x_pos, y_pos):
            pygame.sprite.Sprite.__init__(self)
            self.font =         pygame.font.Font(settings.normalfont, 15)

            self.image = self.font.render("Powered by Forecast", 1, (100,100,100), (0,0,0)).convert()
            self.rect=self.image.get_rect()
            self.rect.topleft=x_pos-self.image.get_width(),y_pos+2
            
    class WindSprite(pygame.sprite.Sprite):
        def __init__(self, x_pos, y_pos):
            pygame.sprite.Sprite.__init__(self)
            self.smallfont =         pygame.font.Font(settings.boldfont, 25)
            self.smallfont_weather = pygame.font.Font(settings.weatherfont, 25)

            self.weatherObject=None

            self.image=pygame.Surface((1,1)).convert()
            self.image.fill((0,0,0))
            self.rect=self.image.get_rect()
            self.x_pos=x_pos
            self.y_pos=y_pos
            self.rect.topleft=self.x_pos,self.y_pos
            self.then=None
            self.hash=None
            self.now = datetime.datetime.now()
            self.sunTime= self.now
            self.sunIcon=None
            self.sunTimeStr=None
            self.width=0
        def update(self, *args):
            if args[0]:
                self.weatherObject=args[0][0]
                self.hash = self.weatherObject.__hash__()
            if self.then!=self.hash:
                self.now = datetime.datetime.now()
                self.sunTime=datetime.datetime.fromtimestamp(self.weatherObject[0])
                self.sunIcon=u"\uF051"
                if (self.sunTime<self.now):
                    self.sunTime=datetime.datetime.fromtimestamp(self.weatherObject[1])
                    self.sunIcon=u"\uF052"
                    if self.sunTime<self.now:
                        self.sunTime=datetime.datetime.fromtimestamp(self.weatherObject[0])
                        self.sunIcon=u"\uF051"
                self.sunTimeStr = (self.sunTime.strftime("%I").strip("0"))
                self.sunTimeStr = self.sunTimeStr+(self.sunTime.strftime(":%M%p"))
                
                self.width,h=self.smallfont.size(self.weatherObject[2]+" mph        "+self.sunTimeStr)
                self.image=pygame.Surface((self.width+35,35)).convert()
                self.image.blit(self.smallfont_weather.render(u"\uF050            "+self.sunIcon, 1, (170,170,170), (0,0,0)).convert(), (0,0))
                self.image.blit(self.smallfont.render(self.weatherObject[2]+" mph        "+self.sunTimeStr, 1, (170,170,170)).convert_alpha(), (35,3))
                self.image.convert()
                self.then=self.hash
                self.rect=self.image.get_rect()
                self.rect.topleft=self.x_pos-self.rect.width,self.y_pos
                
    class CurrentSprite(pygame.sprite.Sprite):
        def __init__(self, x_pos, y_pos):
            pygame.sprite.Sprite.__init__(self)
            self.temp_f =     pygame.font.Font(settings.normalfont, 75)
            self.weather_f =  pygame.font.Font(settings.weatherfont, 50)
            
            self.weatherObject=None

            self.image=pygame.Surface((1,1)).convert()
            self.image.fill((0,0,0))
            self.rect=self.image.get_rect()
            self.x_pos=x_pos
            self.y_pos=y_pos
            self.rect.topleft=self.x_pos,self.y_pos
            self.then=None
            self.hash=None
            self.icon=None
            self.width=1

            self.iconTranslator = defaultdict(lambda: u"\uF03E",
                                              {'clear-day'           : u"\uF00D",
                                               'clear-night'         : u"\uF02E",
                                               'rain'                : u"\uF019",
                                               'snow'                : u"\uF01B",
                                               'sleet'               : u"\uF017",
                                               'wind'                : u"\uF050",
                                               'fog'                 : u"\uF014",
                                               'cloudy'              : u"\uF013",
                                               'partly-cloudy-night' : u"\uF031",
                                               'partly-cloudy-day'   : u"\uF002"})
            
                              
            
        def update(self, *args):
            if args[0]:
                self.weatherObject=args[0][0]
                self.hash = self.weatherObject.__hash__()
            if self.then!=self.hash:
                self.icon=self.weather_f.render(self.iconTranslator[self.weatherObject[4]], 1, (255,255,255), (0,0,0)).convert()
                self.width, h = self.temp_f.size(self.weatherObject[3]+u"\u00B0 F")
                self.image=pygame.Surface((self.width+self.icon.get_width()+15,80)).convert()
                self.image.blit(self.icon, (10,10))
                self.image.blit(self.temp_f.render(self.weatherObject[3]+u"\u00B0 F", 1, (255,255,255), (0,0,0)).convert(), (5+self.icon.get_width()+15,0))

                self.image.convert()
                self.then=self.hash
                self.rect=self.image.get_rect()
                self.rect.height+self.rect.y
                self.rect.topleft=self.x_pos-self.rect.width,self.y_pos

    class ForecastSprite(pygame.sprite.Sprite):
        def __init__(self, x_pos, y_pos):
            pygame.sprite.Sprite.__init__(self)
            self.image=pygame.Surface((1,1)).convert()
            self.text_f = pygame.font.Font(settings.boldfont, 19)
            self.weather_f = pygame.font.Font(settings.weatherfont, 18)
            self.dayAbbr = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
            self.image.fill((0,0,0))
            self.rect=self.image.get_rect()
            self.weatherObject=None
            self.x_pos=x_pos
            self.y_pos=y_pos
            self.rect.topleft=self.x_pos,self.y_pos
            self.iconTranslator = defaultdict(lambda: u"\uF03E",
                                              {'clear-day'           : u"\uF00D",
                                               'clear-night'         : u"\uF02E",
                                               'rain'                : u"\uF019",
                                               'snow'                : u"\uF01B",
                                               'sleet'               : u"\uF017",
                                               'wind'                : u"\uF050",
                                               'fog'                 : u"\uF014",
                                               'cloudy'              : u"\uF013",
                                               'partly-cloudy-night' : u"\uF031",
                                               'partly-cloudy-day'   : u"\uF002"})
            self.then=None
            self.hash=None
            self.temps=None
            self.icon=None
        def update(self, *args):
            if args[0]:
                self.weatherObject=args[0][1]
                self.hash = self.weatherObject.__hash__()
            if self.then!=self.hash:
                self.image=pygame.Surface((200,25*6)).convert()

                for i in range(0,6):
                    self.image.blit(self.weather_f.render(self.iconTranslator[self.weatherObject[i][1]], 1, (255-(i*35),255-(i*35),255-(i*30))).convert_alpha(), (0,i*25))
                    self.image.blit(self.text_f.render(self.dayAbbr[datetime.datetime.fromtimestamp(self.weatherObject[i][0]).weekday()], 1, (255-(i*35),255-(i*35),255-(i*30))).convert_alpha(), (30,3+(i*25)))
                    self.image.blit(self.text_f.render(str(int(round(self.weatherObject[i][2])))+u"\u00B0 F  "+str(int(round(self.weatherObject[i][3])))+u"\u00B0 F", 1, (255-(i*35),255-(i*35),255-(i*30))).convert_alpha(), (100,3+(i*25)))
                
                self.then=self.hash
                self.rect=self.image.get_rect()
                self.rect.topleft=self.x_pos-self.rect.width,self.y_pos
    
    def __init__(self, x_pos, y_pos):
        self.name="WeatherModule"
        self.forecastObject=None
        self.windSprite     = self.WindSprite     (x_pos, y_pos)
        self.currentSprite  = self.CurrentSprite  (x_pos, y_pos+30+5)
        self.forecastSprite = self.ForecastSprite (x_pos, y_pos+30+5+80+15)
        self.poweredBySprite= self.PoweredBySprite(x_pos, 0)
        pygame.sprite.OrderedUpdates.__init__(self, (self.windSprite, self.currentSprite, self.forecastSprite, self.poweredBySprite))
        
class FPSModule(pygame.sprite.OrderedUpdates):
    class FPSSprite(pygame.sprite.Sprite):
        def __init__(self, x_pos,y_pos):
            pygame.sprite.Sprite.__init__(self)
            self.font = pygame.font.Font(settings.boldfont, 25)
            self.image=self.font.render("20", 1, (255,255,255), (0,0,0)).convert()
            self.rect=self.image.get_rect()
            self.x_pos=x_pos
            self.y_pos=y_pos
            self.rect.topleft=self.x_pos,self.y_pos
            self.then = 0.0
        def update(self, *args):
            if args[0]!=self.then:
                self.then=args[0]
                self.image=self.font.render(str(args[0]), 1, (255,255,255), (0,0,0)).convert()
                self.rect=self.image.get_rect()
                self.rect.topleft=self.x_pos,self.y_pos
                
    def __init__(self, x_pos, y_pos):
        self.name="FPSModule"
        self.fpsSprite = self.FPSSprite(x_pos, y_pos)
        pygame.sprite.OrderedUpdates.__init__(self, self.fpsSprite)


class IconModule(pygame.sprite.OrderedUpdates):
    class IconSprite(pygame.sprite.Sprite):
        def __init__(self, x_pos,y_pos):
            pygame.sprite.Sprite.__init__(self)
            self.sprite_check = pygame.Surface([100, 100]).convert()
            self.sprite_listen = self.sprite_check
            self.sprite_nonet  = self.sprite_check
            self.sprite_blank  = self.sprite_check
            self.sprite_active = self.sprite_check
            
            self.sprite_check = pygame.image.load(os.path.join('defaultmodules', 'assets', 'micCheck.png')).convert()
            self.sprite_active = pygame.image.load(os.path.join('defaultmodules', 'assets', 'micActive.png')).convert()
            self.sprite_listen = pygame.image.load(os.path.join('defaultmodules', 'assets', 'micListen.png')).convert()
            self.sprite_nonet = pygame.image.load(os.path.join('defaultmodules', 'assets', 'noNet.png')).convert()
            self.sprite_blank.fill((0,0,0))

            self.image = self.sprite_blank
            
            
            self.rect=self.image.get_rect()
            self.x_pos=x_pos
            self.y_pos=y_pos
            self.rect.topleft=self.x_pos,self.y_pos
            self.then = -1
        def update(self, *args):
            if args[0]!=self.then:
                self.then=args[0]
                if self.then==0:
                    self.image=self.sprite_blank
                elif self.then==1:
                    self.image=self.sprite_listen
                elif self.then==2:
                    self.image=self.sprite_active
                elif self.then==3:
                    self.image=self.sprite_check
                elif self.then==-1:
                    self.image=self.sprite_nonet
                self.rect=self.image.get_rect()
                self.rect.topleft=self.x_pos,self.y_pos
                
    def __init__(self, x_pos, y_pos):
        self.name="IconModule"
        self.iconSprite = self.IconSprite(x_pos, y_pos)
        pygame.sprite.OrderedUpdates.__init__(self, self.iconSprite)


class DateModule(pygame.sprite.OrderedUpdates):
    class DateSprite(pygame.sprite.Sprite):
        def __init__(self, x_pos, y_pos):
            pygame.sprite.Sprite.__init__(self)
            self.month    = ['null','Janurary', 'Feburary','March','April','May','June','July','August','September','October','November','December']
            self.datesprinkle=["th","st","nd","rd","th"]
            self.dayofweek= ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
            self.font =    pygame.font.Font(settings.boldfont, 25)
            self.now = datetime.datetime.now()
            self.then=None
            self.text = ""
            self.image  =  self.font.render(self.text, 1, (170,170,170), (0,0,0)).convert()
            self.rect = self.image.get_rect()
            self.topleft = x_pos,y_pos
            self.rect.topleft=self.topleft            
        def update(self, *args):
            self.now = datetime.datetime.now()
            if self.now.day!=self.then:
                self.then=self.now.day
                sprinkleiter=self.now.day%10
                if (sprinkleiter>4 or self.now.day==11 or self.now.day==12 or self.now.day==13):
                    sprinkleiter=4
                self.text=self.dayofweek[self.now.weekday()]+", "+str(self.now.day)+self.datesprinkle[sprinkleiter]+" "+self.month[self.now.month]+" "+str(self.now.year)
                self.image = self.font.render(self.text, 1, (170,170,170), (0,0,0)).convert()
                self.rect = self.image.get_rect()
                self.rect.topleft=self.topleft
                
    class TimeSprite(pygame.sprite.Sprite):
        def __init__(self, x_pos, y_pos):
            pygame.sprite.Sprite.__init__(self)
            self.font = pygame.font.Font(settings.normalfont, 75)
            self.text=""
            self.now = datetime.datetime.now()
            self.then = None
            self.image  =  self.font.render(self.text, 1, (255,255,255), (0,0,0)).convert()
            self.rect = self.image.get_rect()
            self.topleft = x_pos,y_pos
            self.rect.topleft=self.topleft            
        def update(self, *args):
            self.now = datetime.datetime.now()
            if self.now.minute!=self.then:
                self.then=self.now.minute
                self.text = self.now.strftime("%I")
                if (self.text[0]=="0"):
                    self.text=self.text[1:]
                self.text = self.text+(self.now.strftime(":%M"))
                self.image = self.font.render(self.text, 1, (255,255,255), (0,0,0)).convert()
                self.rect = self.image.get_rect()
                self.rect.topleft=self.topleft

    class SecondsSprite(pygame.sprite.Sprite):
        def __init__(self, x_pos, y_pos, offset):
            pygame.sprite.Sprite.__init__(self)
            self.font = pygame.font.Font(settings.normalfont, 25)
            self.text=""
            self.now = datetime.datetime.now()
            self.then = None
            self.image  =  self.font.render(self.text, 1, (255,255,255), (0,0,0)).convert()
            self.rect = self.image.get_rect()
            self.offset=offset
            self.x_pos=x_pos
            self.y_pos=y_pos
            self.rect.topleft=self.x_pos+self.offset,self.y_pos      
        def update(self, *args):
            self.offset=args[0]
            self.now = datetime.datetime.now()
            if self.now.second!=self.then and self.offset>100:
                self.then=self.now.second
                self.text=str(self.now.second).rjust(2,'0')
                self.image = self.font.render(self.text, 1, (255,255,255), (0,0,0)).convert()
                self.rect = self.image.get_rect()
                self.rect.topleft=self.x_pos+self.offset,self.y_pos
                
    def __init__(self,x_pos, y_pos):
        self.name="DateModule"
        self.now = datetime.datetime.now()
        self.date    = self.DateSprite   (x_pos, y_pos)
        self.time    = self.TimeSprite   (x_pos, y_pos+40)
        self.seconds = self.SecondsSprite(x_pos+7, y_pos+53, self.time.image.get_width())
        pygame.sprite.OrderedUpdates.__init__(self, (self.date, self.time,self.seconds))        
    def update(self, *args):
        pygame.sprite.OrderedUpdates.update(self, self.time.image.get_width())

