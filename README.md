digitalMirror
=============

A customizable framebuffer dashboard for the Raspberry Pi. Intended for mounting behind a one-way mirror.
